EdDSA Ed25519/Ed448 for XML Digital Signatures
==============================================

We describe how EdDSA digital signatures (Ed25519 and Ed448) [EDDSA]
can be used in XML Digital Signatures [XMLDSIG].

Approach
========

EdDSA is used in XML Digital Signatures through the Elliptic Curve
Digital Signature Algorithm (ECDSA) framework described in [XMLECDSA].

While EdDSA is different from ECDSA, we have chosen to re-use existing
elements and specify semantic conventions specific to EdDSA, rather
than to describe a new mechanism.

The remaining sections describe how use of EdDSA differs from use of
other ECDSA schemes in the [XMLECDSA] framework.

Namespace and Identifiers
=========================

The XML namespace URI that MUST be used by implementations of this
specification is:

    http://josefsson.org/2015/12/xmldsig-eddsa#

Elements in the namespace of the [XMLDSIG] specification are marked by
using the namespace prefix "dsig" in the remaining sections of this
document.

The URI Identifiers used to identify the algorithms we describe are
for Ed25519 and Ed448 respectively:

    http://josefsson.org/2015/12/xmldsig-eddsa#ed25519
    http://josefsson.org/2015/12/xmldsig-eddsa#ed448

EdDSA Key Values
================

EdDSA public keys are encoded using the new element EdDSAKeyValue.
Include the element inside dsig:KeyValue similar to dsig:RSAKeyValue,
dsig:ECDSAKeyValue.  The EdDSA public keys are binary strings as
described in [EDDSA], here encoded as base64.

DTD Replacement
---------------

The following definition amends the entity Key.ANY

    <!ENTITY % KeyValue.ANY '| ecdsa:EdDSAKeyValue'>

Schema definition
-----------------

The element consists of the base64 encoded public key.

      <xs:element name="EdDSAKeyValue" type="eddsa:EdDSAKeyValueType"/>
      <xs:simpleType name="EdDSAKeyValueType">
           <restriction base="base64Binary"/>
      </xs:simpleType>

DTD Definition:

      <!ELEMENT EdDSAKeyValue  (#PCDATA)  >
      <!-- base64 encoded digest value -->

EdDSA Signatures
----------------

Similar to RFC 4050, the input to the EdDSA algorithm is the
canonicalized representation of the dsig:SignedInfo element as
specified in Section 3 of [XMLDSIG].

The output of the EdDSA algorithm is a binary string.  The signature
value (text value of element dsig:SignatureValue - see section 4.2 of
[XMLDSIG]) consists of the base64 encoding of the binary EdDSA output.

Security Considerations
=======================

See [EDDSA], [XMLDSIG], and [XMLECDSA]

XML is by design prone to implementation flaws.

References
==========

[EDDSA] https://tools.ietf.org/html/draft-irtf-cfrg-eddsa

[XMLDSIG] RFC 3275

[XMLECDSA] RFC 4050

Appendix
========

Aggregate XML Schema
--------------------

    <?xml version="1.0" encoding="UTF-8"?>
    <xs:schema
     targetNamespace="http://josefsson.org/2015/12/xmldsig-eddsa#"
     xmlns:ecdsa="http://www.w3.org/2001/04/xmldsig-more#"
     xmlns:xs="http://www.w3.org/2001/XMLSchema"
     elementFormDefault="qualified"
     attributeFormDefault="unqualified"
     version="0.2">

     <!--ECDSA key value root element-->

     <xs:element name="EdDSAKeyValue" type="eddsa:EdDSAKeyValueType"/>
     <xs:simpleType name="EdDSAKeyValueType">
          <restriction base="base64Binary"/>
     </xs:simpleType>

    </xs:schema>

Aggregate DTD
-------------

    <!ELEMENT EdDSAKeyValue  (#PCDATA)  >
    <!-- base64 encoded digest value -->
